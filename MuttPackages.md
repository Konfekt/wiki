This page contains links to packaged versions of Mutt in various OS
distributions.

# Fedora

<http://pkgs.fedoraproject.org/cgit/mutt.git/tree/>

# Debian

<http://anonscm.debian.org/gitweb/?p=pkg-mutt/mutt.git;a=tree;f=debian/patches;h=001cba693d8880724697043c10d833be9e4db387;hb=master>

# Gentoo

* Gentoo's clone with transplants of non-features in gentoo-1.5.21 branch:  
  <http://prefix.gentooexperimental.org:8000/mutt/graph>  
* Gentoo's patches on top of that as Mercurial patchqueue  
  <http://prefix.gentooexperimental.org:8000/mutt-patches/file>

# FreeBSD

* <http://svnweb.freebsd.org/ports/head/mail/mutt/files/>
