### How can I search multiple Maildirs for strings all at once?

Use **grep** or others. Mutt doesn't support a recursive search. Or use
Mairix. It indexes the mails in the maildir folders. When a search is
invoked, it puts symlinks for the found mails in a designated folder. It
can be invoked using "!" within Mutt. See
<http://www.rc0.org.uk/mairix> for
more.

### Which mailbox format is better - [mbox](http://www.qmail.org/man/man5/mbox.html) or [Maildir](http://www.qmail.org/man/man5/maildir.html)?

* <http://cr.yp.to/proto/maildir.html> (technical advantages)  
* <http://www.courier-mta.org/mbox-vs-maildir/> (performance advantages)

Decide on your own which one best fits your needs. See also
[MaildirFormat](MaildirFormat) and [Extended[MaildirFormat](MaildirFormat)](Extended[MaildirFormat](MaildirFormat)).

### What is the best directory layout for Maildir folders?

There are two ways, some favour the first, some the second, again decide
for your own:

#### 1\. ExtendedMaildirFormat

Supported by Courier, dovecot and probably other IMAP servers and
apparently Kmail (it works yet Kmail's method of saving drafts and sent
mail is slightly different but compatible) this allows you to easily
share your folders with another MUA or an IMAP server to access
remotely. (Remember that Maildir allows simultaneous access without the
need for locking).

See [ExtendedMaildirFormat](ExtendedMaildirFormat) for example layout and how to create folders.

To get mutt to work nicely with this setup use the following in your
muttrc:

``` 
 set mbox_type=Maildir

 set spoolfile="~/Maildir/"
 set folder="~/Maildir/"
 set mask=".*"    # the default mask hides dotfiles and maildirs are dotfiles now.
 # set mask="!^\.[^.]"  # this line intentionally commented out
 set record="+.Sent"
 set postponed="+.Drafts"

 mailboxes ! + `\
 for file in ~/Maildir/.*; do \
   box=$(basename "$file"); \
   if [ ! "$box" = '.' -a ! "$box" = '..' -a ! "$box" = '.customflags' \
       -a ! "$box" = '.subscriptions' ]; then \
     echo -n "\"+$box\" "; \
   fi; \
 done`

 macro index c "<change-folder>?<toggle-mailboxes>" "open a different folder"
 macro pager c "<change-folder>?<toggle-mailboxes>" "open a different folder"
```

(Note: When copy/pasting this, make sure you remove whitespace from the
end of the lines as otherwise the backslashes at the ends of the lines
will not work, resulting in a syntax error.)

The '!' adds the user's default mail spool location, '+' the base
folder to the list of mailboxes. This may be necessary if, for example,
the system uses mbox. The above code assumes your Maildir being
~/Maildir and containing Drafts and Sent subfolders as commonly used by
other MUAs via IMAP. It uses the mailboxes feature to make the folder
browser work, ignoring the .customflags and .subscriptions files dovecot
puts into Maildirs.

Note that there should be no trailing spaces behind the backslashes
('\\') at the end of the lines, blame the Wiki and remove them after
pasting or you will see errors like "echo: unknown command"! Also
ensure that the lines mailboxes\` and done\` are on the same line such
that without the backslashes so that it looks like

``mailboxes ! + `... done` ``

To additionally get to the folder browser when copying and moving mail,
add the
following:

``` 
 macro index C "<copy-message>?<toggle-mailboxes>" "copy a message to a mailbox"
 macro index M "<save-message>?<toggle-mailboxes>" "move a message to a mailbox"
```

#### 2\. custom layout of separate Maildirs

First note that there are *directories* and *folders* and that an item
cannot be both in Mutt. This means that Maildir folders (which are Unix
directories) **should not** be used to store subfolders. If you want
subfolders, create folders in subdirectories (which in turn do **not**
contain messages themselves).

Let's make an example: there are folders *peter*, *mike*, *mutt*, and
*debian*. A flat structure would like the following:

``` 
 ~/''''Mail/
    peter/
      cur/ new/ tmp/
    mike/
      cur/ new/ tmp/
    mutt/
      cur/ new/ tmp/
    debian/
      cur/ new/ tmp/
```

To group things, create new directories. If you have messages that fit
into a category, but not into a sub-category, create a new folder, e.g.
*misc*.

``` 
 ~/''''Mail/
    friends/

      peter/
        cur/ new/ tmp/
      mike/
        cur/ new/ tmp/
      others/
        cur/ new/ tmp/
    software/
      mutt/
        cur/ new/ tmp/
      debian/
        cur/ new/ tmp/
      misc/
        cur/ new/ tmp/
    archive/
      cur/ new/ tmp/
```

  - again: don't reuse folders as directories!
  - if possible, make sure that all directories and folders start with a
    different letter to make them <tab>-friendly

*So far for the theory part.* The whole truth is that you can indeed
create subfolders inside Maildir folders. The problem with that is that
Mutt's directory browser cannot know whether you want to enter the
folder when you press enter on the item, or whether you want to go to
the subdirectory to see the subfolders there. You can still enter the
folder's full name at the 'c'hange folder prompt, but the folder browser
will not work.

### I can see size in bytes, but not lines, is there a fix?

Maildir does not produce the required header lines to support this.
However, if you use procmail, you can add it yourself! Add the
following code to your .procmailrc:

```
# MAILDIR LINES
# Add Lines header for mutt with Maildirs. Mutt shows lines based on Lines
# header.  We have to do this manually for Maildir delivered messages.
:0 BWfh
* H ?? !^Lines:
* -1^0
*  1^1 ^.*$
| formail -A "Lines: $="
```

### I have a whole Maildir folder structure, how can I get it to show up in Mutt?

This script is a simple modification of the one provided above. The
difference is that it scans one level deeper into the folder
structure--it also makes the root of the maildir structure only show up
once instead of twice in the list. Note that this assumes that your
maildir is "~/.maildir"

```sh
 mailboxes + `\
 for file in ~/.maildir/.*; do \
   box=$(basename "$file"); \
   if [ ! "$box" = '.' -a ! "$box" = '..' -a ! "$box" = '.customflags' \
       -a ! "$box" = '.subscriptions' ]; then \
     echo -n "\"+$box\" "; \
   fi; \
done; \
 for folder in ~/.maildir/*; do \
   if [ -x $folder]; then \
         box=$(basename "$folder"); \
         for file in ~/.maildir/$box/.*; do \
                box2=$(basename "$file"); \
                if [ ! "$box2" = '.' -a ! "$box2" = '..' -a ! "$box2" = '.customflags' \
                 -a ! "$box2" = '.subscriptions' ]; then \
                   echo -n "\"+$box/$box2\" "; \
                fi; \
         done; \
    fi; \
  done`
```

(Note: When copy/pasting this, make sure you remove whitespace from the
end of the lines as otherwise the backslashes at the ends of the lines
will not work, resulting in a syntax error.)
