# Issues to be resolved before the next point release (1.5.21)

* IMAP delimiter bug (#3242)  
* Some progress on unified [NewMailHandling](NewMailHandling) (see #3271)

## IMAP delimiter bug

1. Identify code that mangles URLs or inserts folder delimiters  
1. Figure out which of that code runs before a delimiter is known.

### Desired behaviors

* `mailboxes +foo` where `$folder` is an IMAP folder, e.g., `imap://server/INBOX` should use the correct delimiter between INBOX and foo. The trick is that this is expanded before a connection is opened to get the delimiter for INBOX. Possibly we could add a 'not normalized' bit to the buffy list, and do the glue after the connection has been opened.

In general, the problem is that `mutt\_expand\_path` may be called
before a connection is open, and it returns a simple string, so it's
hard to signal that we don't have a real delimiter
yet.

### Possible strategies

* For IMAP urls, keep a separate table for entries where the delimiter isn't yet known, that records the original, unexpanded path and the guessed expansion. When it sees a URL matching the guessed expansion, it can go back to the unexpanded form and reexpand it with the known delimiter. Complicated.  
* Defer path expansion until just before the path is actually accessed. This provides robust expansion, but if the definition of $folder changes, how do we make sure the version that was in effect at the time of the definition was used?  
* Perhaps we need to use something with more information than a simple string as a mailbox path. This would be invasive though.
