If you have questions that cannot be answered by RTFM, join one of the
lists and ask there. If you like, stay some time to help others by
answering some questions yourself. Or even stay permanently, maybe even
join [MuttMaps](MuttMaps). :-)

-----

## Lists Guidelines

**REQUIRED: read [BeforeYouAsk](BeforeYouAsk)**

If you don't, then the help you get will not be as good (speed +
quality) as it could be!

All the lists have **ARCHIVES**! Search them for your request, it
probably has been asked and answered before!

For development requests: <http://marc.info/?l=mutt-dev>

For users: <http://marc.info/?l=mutt-users>

For Usenet news it's the google archive via the below given
web-interface.

**REQUIRED: read [BeforeYouAsk](BeforeYouAsk)!!!**

See **technical below** at the end, too!!!

-----

## Where to post

### original mailing lists "@mutt.org"

A mail sent to a central server, receive all/ download first, select
thereafter.

* must subscribe with valid email-address before you post.
* to subscribe, unsubscribe, or manage your access to the list, see
  <http://www.mutt.org/mail-lists.html>.
* send mail to list with valid From:-addr to the `"list-name" @
  mutt.org` **_after_** you checked the resources from the guidelines
  above!

**mutt-users**: general purpose for users who can't help themselves by
reading docs alone.

* english only, max size 20KB/post.  
* **mutt-users-fr**: french only, much smaller group.

**mutt-dev**: discussion of code functionality rather than usage and
configuration, which belongs to
mutt-users.

* english only, max size 50KB/post (for minor patches and log excerpts).  
* bug reports are automatically posted here.

**mutt-announce**: for information about official releases.

* it's one-way, read-only, you can't post yourself.

### <news://comp.mail.mutt>

A mutt-general "newsgroup" == download only what you
choose.

* recommended: read with a proper news-reader like [tin](http://www.tin.org/), [slrn](http://www.slrn.org/) or emacs gnus.  
 * last resort: via <http://groups.google.com/group/comp.mail.mutt/topics>, (but please use a real reader, please!!!).  
 * this is also the **ARCHIVE** for this group, if your ISP doesn't provide all topics: some offer only articles of a few weeks in the past.  
* access: when client setup properly with your newsfeed (ask your ISP for help), then simply post **_after_** you read the guidelines below.  
 * post without special registration/ subscription, posting limits per your ISP.

All lists have separate users: if you want to reach all, then you have
to join all.

-----

## Technical Lists

When you post: \*be considerate of the readers\* Your chances increase
significantly if you
...

* use **meaningful** "Subject:", so that people quickly can see how to help you.  
* do _NOT_ "top-post" (also called TOFU), this means: in a reply ...  
 * **quote only
passages** that you refer to, delete unreferenced parts.  
  * if you don't refer to any specific quoted passage, then don't quote at all but summarize what you reply to.  
 * add your **responses after each quoted
passage** that you refer to.  
  * do not quote the full text as whole (unless it's all needed) and do not place your responses above the quotes.  
* send only plain text messages: don't post text with or only HTML (multipart/ alternative) from web-mailers.

Special for the mailing
lists:

* use mutt's ` <list-reply> ` feature to respond to the lists only, not to senders, too (unless asked for), i.e. list-reply, not group-reply.  
 * only subscribers can post, no need to reply to _both_ the list and the sender, just reply to the list, sender won't miss it!  
 * however, when you reply to bug reports in mutt-dev, reply only to the bug sender address, it will autoforward your response to mutt-dev.  
* if you subscribe with an address that is not reachable for whatever reason for a longer period, then you will be **automatically
unsubscribed**.

### encryption/ signing

Even though authentification is generally a good idea and recommended,
in practice it isn't widespread yet. Think about this before you enable
or disable it for the
lists:

* is it required to authenticate your post?  
 * somebody poses as you?  
 * is the post really so sensitive?  
 * on the other side, signing everything helps establish the key.  
* has everybody on the list your keys and actually verifies?  
 * otherwise some find it wasted load.  
 * on the other side: if nobody signs, it will never become established for general use.
