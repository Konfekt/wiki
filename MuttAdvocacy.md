## mutt is for those willing to put out some effort and learn something

Mutt is not designed to suit *everyone*, including those without basic
knowledge about the [MailConcept](MailConcept), or those unwilling to perform
configuration. Mutt is also not designed for the mass of "average
users", although it works well for them too. For those who prefer a
client to "just work *somehow*" rather than give dedicated best
performance as Mutt does, they are probably better suited to use other
mail programs.

If you spend a little more time to set it all up properly, it will
quickly pay off in your daily work. What you save with "ready-to-use"
all-in-one tools at the beginning, you lose later compared to the power
of Mutt and its tools. While mutt's defaults might satisfy basic use, to
adapt it well to your personal needs you must put in a little extra
effort. This includes compilation setup and run-time configuration. Also
employing the proper suite of tools to add functionality a MUA is not
intended to provide.

If you need MTA functionality, use an MTA. Generic MIME handling? Try
mailcap. If you need encryption use an encryption program. A mixed-use
client typically won't offer as good, capable, easy to use, secure, and
efficient an implementation of the desired functions as a dedicated
tool.

-----

Here you can find some reviews of other sites to help you decide whether
Mutt is worth the effort to get started with it. If you find some new
ones, append
them.

`* A Man And His Mutt: `<http://www.melonfire.com/community/columns/trog/article.php?id=98>  
`* Mark Stosberg's Mutt Fan & Tip Page: `<http://mark.stosberg.com/Tech/mutt.html>  
`* Jack Mottram on Mutt: `<http://lifehacker.com/5574557/how-to-use-the-fast-and-powerful-mutt-email-client-with-gmail>  
`* Sukrit Dhandhania `<http://www.linuxuser.co.uk/tutorials/get-started-with-mutt>
