[PageOutline](PageOutline "wikilink")

## Intro

Please add your IRC-nick or some other unique means (e.g.
\[nick\]name+location where you read about this) to identify your vote.
You may append a comment per "\` \* nick: comment\`", if comment gets
very long, continue on extra line with "\` \* more comment\`".

**DO NOT DISCUSS ARGUMENTS HERE**! That's what [VarNames/Discussion](VarNames/Discussion) is
for! Anything that is NOT or **NOT SUFFICIENTLY covered** on
/Discussion (to make you vote "yes"), bring it on ... either on
mutt-users Mailing-List or add it yourself to that /Discussion page
**there**, not here!
As comment please add **just
the titles** of the points from /Discussion **which** made you vote as
you did (or otherwise SHORT comment)!

**Read FIRST + ALL** before you write. Save **YOUR TIME** for not
writing and **OUR TIME** for not having to read what has already been
covered!

* Don't repeat: a reason is not more valid when you repeat.  
* Don't attach your name to a reason or vice versa: reasons are not more valid when made by any specific person.

-----

**IMPORTANT: There is no SHOUTING here!**

What you might mistake for SHOUTING is just emphasis for those people
who have proven not to have the required attention span to read monotone
text carefully and completely, and therefore either don't understand or
simply ignore already given responses or instructions they missed
before.

If you are not one of those, then please just ignore the style and focus
on the content. The others obviously need the clues to what is
    relevant.

-----

    The goal of this poll is NOT to figure out WHETHER it helps or not or HOW MUCH it helps!
    The goal is to learn whether YOU PERSONALLY will accept the one-action change.
    DO NOT SPOIL THE RESULT BY a vote in SPECULATION of a result or to PROTECT others!
    Vote just for yourself alone and not anybody else: DON'T SPECULATE ABOUT OTHERS.

-----

Don't vote "it will hurt others" who don't participate themselves,
**don't speculate** what a majority **should** be to side with it.  
Such reasoning spoils the purpose of this inquiry:
to **stop speculation** and find out the **real** numbers of those who
speak their opinion.
Those others **are** considered
by the reasons on /Discussion! Check them out.
Give
them your reasons **THERE**, not your vote **HERE**. Let them vote for
themselves!

**Have your \_OWN\_ opinion and just vote for YOURSELF!**

|| If you vote **"YES"**, then you ...|| If you vote **"NO"**, then you
... || || Improve docs, share good software with newbies || Preserve
your OWN config ALONE and keep to yourself "tough guys" || || Make it
easier for Newbies than when you learned it || Want newbies to go
through same pain and mistakes as you did || || Support those who must/
want use the manual in the future || Support those who are lucky not to
need the manual anymore || || Can live (fine) with a one-time change of
your config || CAN NOT live with a one-time change of your config ||

**Note:** "stable" users have to change some stuff already anyway when
UPDATING, "dev" users have the natural risk to deal with above average
changes.

READ [VarNames/Discussion](VarNames/Discussion) BEFORE you
vote!

-----

-----

## No, I prefer to keep current variable names

1. klieber: it's not broken, don't fix it.  
1. Chris Green: quite agree, changes will require endless dicussion anyway  
 a. Rado: the code is not broken, but the manual is: software not only about code! see /Discussion to end it there.  
1. Luke Ross: By all means break the manual into sections, but I don't see the need to rename the variables, too. Sorry, I just don't buy it.  
1. Gary Johnson: Reorganize the manual and categorize variables enough, names don't matter (with good or bad docs).  
   Development aware users could change from old to new names without too much hassle, but ordinary user would be confused and ticked off.  
 a. Rado: please consult /Discussion: "matter" is covered, ordinary users are calmed by /Script.  
1. parv@pair: Even though i could bear the one time change, I must use different mutt versions.  
   In that case, i would need to maintain two different configurations, which I do not want to look forward to.  
1. James Wilkinson: Please don't. I agree that reorganising the manual makes sense, but that should stand on its own merits.  
   Longer var names intimidate users looking at examples and make them harder to remember.  
   If you want to rename variables, then do it because the new names inherently are easier to use.  
 a. Rado: see /List + /Discussion, they are.  
1. Michael Tweedale: This sounds like a disastrous idea! How many thousands of configurations will be broken?  
   The names are already sufficiently descriptive, and good documentation plus a well-commented sample muttrc file are all that's needed.  
1. Paul: short and sweet - I agree with Gary and Michael. But if we must do this, at least make it mutt 2.0.  
1. vinc17: Reorganizing the manual should be sufficient. I must use different versions of Mutt, this is not just a matter of .muttrc rewrite.  
   If variable names are too complicated for some users, new names won't be sufficient; they probably need a configuration tool, where variable names don't matter.  
   Categories are limited; a set of keywords associated with each variable could be more useful, and a category prefix won't make much sense any longer.  
 a. Rado: this has been covered on /Discussion: having a tool doesn't make a good manual useless.  
1. Pete Johns: While I'm confident that I can migrate my .muttrc succesfully, [arguments moved to /Discussion],  
   broken configurations would mean that I would have to spend my time fixing other people's .muttrcs.  
   Personally, I am in favour of improving the documentation over breaking existing configurations.  
   I vote for Personal Convenience in this case as I remain unconvinced that the Improvement is Global. I thank the maintainers whatever they decide to do.  
1. em@il.unwired.homeunix.net: I have spent the first couple of years using mutt getting the config to do the right thing [tm],  
   I haven't had to make any alterations for the last couple of years, and as such have forgotten what alot of it does :-),  
   i really don't want to go through it all again. [This is selfish, but you did ask for personal opinion]  
 a. Rado: remember, some changes "alternates/ envelope_from" will come anyway, and the "simple" cases are covered by /Script.  
1. brianb: Given the voting options, I must choose No. I dont believe an alternative which preserves backwards compatibility is beyond reach.  
 a. Rado: backward compatibility generally is no problem with synonyms, just once the old/ ancient strings must be cut off to keep/ get a clean state.  
1. Brian Salter-Duke. I do agree with Gary Johnson who has think has said many wise things about mutt over the years and done other good things.  
   I do think it will hurt newbies and I resent being preached at by the proposer of this idea. Rado, just shut up and let the debate continue.  
 a. Rado: GJ is free to decide for himself, but neither he nor anybody else should vote for others, just for themselves.  
    This is not about "will it hurt _them_", but does it hurt _you_!  
    It's about finding real problems that can be fixed rather than speculation about others and resulting paranoia.  
1. eoghan: No way. Causes too much hassle and confusion for not enough gain.  
1. Frank Hart: don't see the point at all. And what's with all the shouting?  
 a. Rado: the intro is changed so that you hopfefully better understand.  
1. Ville: The shouting irritated me so much that I just have to say no.  
 a. Rado: sorry... plain/text has not many alternatives for emphasis.

-----

## Yes, let's apply the naming scheme

1. Rado: I came up with it, so ... I understand all being said in /Discussion, and think it's worth it, since the costs are low now with a working /Script.  
1. Daniel_Bescheid_Germany: Well, why not  
1. festus: - go for it - just document it well  
1. p@rick: A very welcome idea. Go for it!  
1. Ataualpa: I agree but, please, let guides-writers (like me) time to rewrite!  
1. Ketju: I've no problem changing to assist others learn the wonder that is mutt; but [can have both versions in transition]? Yes, see main-page.  
1. Gufo: It's a nice idea, [...] [have short transition] time [...] it's better to keep the old schema for compatibility along with the new one.  
1. Gero T: do it, and do it thoroughly, if on the way  
1. zenatode: Nice idea, the sooner the better  
1. aamehl: Mutt doesn't need to be archaic to be useful. The variables are confusing.  
   I struggled to set everything up the way I wanted it and then haven't touched it. When I change my system and need to change my configuration I don't enjoy myself.  
   YES make it clearer, YES make it easier. The seasoned users will complain but once the pain is over they too will enjoy a clearer cleaner way of doing things.  
   [ off-topic request taken off: "better defaults support and easier configuring system" ]  
1. kyle@memoryhole: I conditionally approve. I'm fine changing my config, but such a change must be *managed* properly (with backward compatability, ...)  
 a. Rado: it will be.  
1. will@yellow: Lots of good arguments for and against, but on balance I think go for the change.   
1. sridhar: Go for it  
1. sky: I am convinced. This is good for "future" mutt -or- If we want to see widely used mutt in the future then go for it!  
   **BUT** I guess the new Varnames should go to a new config file for compatibility / stability / coding reasons  
   (e.g .mutt but definitely not .muttrc2 - who is going to change this name to .muttrc after all is stable).  
 a. Rado: there is section 3 of manual for multiversions. BTW: Sky, you dummy, keep voting comments here at your vote, not all over the page! ;-)  
1. mkelly: Yes. Sounds like a big win in the long run.  
1. Till Maas: Progress is good  
1. Jerome: definitly go for it. Making configuration variable names more explicit just makes mutt mutter.  
1. wp: Keeping compatibility slows down improvements. It's a piece of cake for me to upgrade.  
1. redondos: I think it would be a step forward, and it won't hurt veterans if they are properly warned and there is a decent transition time.  
   Of course, also with the help of the /Script, which seems to work fine for my configuration.  
1. Kyrre Nygård (awad): The only thing constant is change, it's about time Mutt got itself out of this complex rut and moved towards greater usability.  
   Anything which will simplify Mutt and make it easier to use is most welcome in my humble opinion. Good design makes things better for people.  
   Good design takes something complex and makes it easy. Good design should be emphasized more in good software like Mutt.  
   After all, there is a nice future awaiting Mutt, just think about the vast amount of new users about to come on to the Internet -- not just poor people -- but our kids too! 

-----

## I care, but still I'm undecided

1. sarnold: I'd welcome any change that lessens the amount of time I spend helping other people try to find the exact configuration option they want;  
   however, [ setup with different mutt binary versions ] [ request for support different configs per version ]  
   If there was a method to #if muttversion > 7 <new config> #else <old config> #end or some similar mechanism  
   to allow a single .muttrc to work for both old and new systems, I'd be more inclined to support the renaming.  
 a. init0: there is sort of - mutt will read .mutt-$version if that exists. See <https://muttmua.gitlab.io/mutt/manual-dev.html#configuration-files>  
1. kst: I have no strong opinion about the change itself. My biggest concern is, of course, backward compatibility.  
   There's always the possibility that some users will need to use the same .muttrc file for multiple versions of mutt.  
   I suggest allowing the use of a new name for a new-style .muttrc file, perhaps .muttrc2. Older versions of mutt will obviously look only at .muttrc.  
   Newer versions that implement the new names can read .muttrc2 if it exists, falling back to .muttrc if it doesn't.  
   There should definitely be a tool to translate an old-style .muttrc file to a new-style .muttrc file; if I have to edit the file manually, I'll vote no on the whole idea.  
 a. Rado: see above, covered by section 3 of manual. Syntactical differences exist already between stable version. The latter good ideas will be added to list of solutions.  
  1. Given the .muttrc and .muttrc2 files, the new version of mutt that implements this could do the following on startup:  
     IF .muttrc exists AND .muttrc2 doesn't exist AND .muttrc is an old-style file THEN Automatically generate a .muttrc2 file from the existing .muttrc file.  
   b. Rado: mutt never created config, but if it once would, it will be a good idea.  
1. max: I support the previous messages. Increment the major version and make the new mutt-2 read .mutt2rc as a new format, and .muttrc as an old format.  
1. jerri: this might have potential. but on the other hand a change at this point of development really could break a lot of things.  
   In my opinion a new system for the configuration should be proposed and then checked.  
 a. Rado: this is what this vote is about!!! :-)  
  1. Maybe it is possible to change the configuration-system in a way to be backward compatible. Right now I see nothing wrong with the current configuration-system.  
   b. Rado: see above and /Discussion!  
1. pdmef: The direction is right since the documentation as a whole needs lots of improvements. Though I'm undecided, there're some things which could make me vote yes.  
   We need to do this change in a branch and re-work the manual so what gets committed must be a huge update make things more consistent in total at once.  
   We need to rewrite the manual (not just re-organize) to reflect groups of "tasks" and such.  
   We need to collect a list of package maintainers for nearly any more or less popular system an mail them long before so they can contribute ideas for the migration.  
   We need to have a detailed plan for migration before we start implementing the changes.  
   We need lots of publicity to spread the word, i.e. mutt 2.0 could possibly make the media catch up (so that people can read about it in the news, not when updating).

-----

## I don't care either way

1. andy chambers: there's a script - I can run it  
1. ssmeenk <freshdot.net>: there's a script - I can run it too ;)

-----
