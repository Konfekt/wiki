# Show mail in GUI App on Windows

To be able to view an E-Mail from [Mutt](https://gitlab.com/muttmua/mutt/) under Linux or Windows (using the WSL Subsystem for Linux)

- in your GUI E-Mail client (by default [thunderbird](https://community.chocolatey.org/packages/thunderbird) in its default installation folder on Windows 64 bit, but customizable by the environment variable `$EMAIL`), select the e-mail in the index and hit `E`(mail),
- in your GUI browser (by default [firefox](https://community.chocolatey.org/packages/firefox) in its default installation folder on Windows 64 bit, but customizable by the environment variable `$BROWSER`), select the `HTML` attachment in the attachment viewer and hit `B`(rowser),

add to `~/.muttrc` the lines

```sh
macro pager  E        "<pipe-message>iconv -c --to-code=UTF8 > ~/.cache/mutt/mail.eml<enter><shell-escape>`echo ""\""${EMAIL:-thunderbird}""\""` $HOME/.cache/mutt/mail.eml<enter>"
macro attach B        "<pipe-entry>iconv -c --to-code=UTF8 > ~/.cache/mutt/mail.html<enter><shell-escape>`echo ""\""${BROWSER:-firefox}""\""` $HOME/.cache/mutt/mail.html<enter>"
source `if [ -z "${WSLENV+x}" ]; then FILE=/dev/null; else FILE="$HOME/.config/mutt/wsl"; fi; echo "$FILE"`
```

and save a file `~/.config/mutt/wsl` containing the lines

```sh
macro pager  E        "<pipe-message>iconv -c --to-code=UTF8 > ~/.cache/mutt/mail.eml<enter><shell-escape>`echo ""\""${EMAIL:-$ProgramFiles/Mozilla Thunderbird/thunderbird.exe}"\"""` `wslpath -w $HOME/.cache/mutt/mail.eml | sed 's@\\@\\\\@g' 2>/dev/null` 2>/dev/null<enter>"
macro attach B        "<pipe-entry>iconv -c --to-code=UTF8 > ~/.cache/mutt/mail.html<enter><shell-escape>`echo ""\""${BROWSER:-$ProgramFiles/Mozilla Firefox/firefox.exe}"\"""` `wslpath -w $HOME/.cache/mutt/mail.html | sed 's@\\@\\\\@g' 2>/dev/null` 2>/dev/null<enter>"
```
